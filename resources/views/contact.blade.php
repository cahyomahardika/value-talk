<?php $nav_contact = 'active'; ?>

@extends('master')

    <!-- content -->
@section('title','Kontak')
      <!-- sect 1 -->
@section('content')
      <form action="/contact/store" method="post">
      {{ csrf_field() }}
        <div class="container-fluid h80-vh d-flex align-items-center mb-5">

          <div class="container">

            <div class="row">

              <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
                <h1 class="display-4 mb-4">Kontak</h1>
                <img src="/assets/images/content/kontak.png" class="img-fluid">
              </div>

              <div class="col-12 col-sm-12 col-md-6 col-lg-9 col-xl-7 offset-xl-1">

                  <div class="form-group">
                    <input type="text" class="form-control" name="name" required="required" placeholder="Nama Lengkap">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" required="required" placeholder="Email">
                  </div> 
                  <div class="form-group">
                    <input type="number" class="form-control" name="phone" required="required" placeholder="Nomor HP">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" name="message" rows="3" required="required" placeholder="Pesan Anda"></textarea>
                  </div>
                  <input class="btn btn-primary btn-lg btn-daftar-1 btn-kirim ml-2 mt-4"  type="submit" value="Kirim"></a>

              </div>

            </div>

          </div>

        </div>

      </form>

      <!-- sect 1 -->

    <!-- content -->
@endsection
