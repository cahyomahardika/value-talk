<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,400;0,600;0,700;0,800;1,400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="shortcut icon" href="/assets/images/logo/value-talk.ico">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>@yield('title')</title>
  </head>
  <body>
  @include('sweet::alert')
    <!-- header -->

    <header class="container-fluid mb-5">

      <div class="container">

        <nav class="navbar navbar-expand-lg navbar-light bg-white pt-4 pl-0 pr-0">

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <a class="navbar-brand nav-logo" href="/site">
            <img src="/assets/images/logo/valuetalk.svg" alt="ValueTalk" class="img-logo img-fluid">
          </a>

          <div class="collapse navbar-collapse mt-2" id="navbarSupportedContent">
            <ul class="navbar-nav nav-value ml-auto">
              <li class="{{ $nav_home ?? '' }} nav-item mt-auto mb-auto">
                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="{{ $nav_contact ?? ''  }} nav-item mt-auto mb-auto">
                <a class="nav-link" href="/contact">Kontak</a>
              </li>
              <li class="{{ $nav_terms ?? ''  }} nav-item mt-auto mb-auto">
                <a class="nav-link" href="/termscondition">Persyaratan dan Kondisi</a>
              </li>
              <li class="{{ $nav_registration ?? ''  }} nav-item mt-auto mb-auto">
                <a class="nav-link" href="/registration">Daftar</a>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0 d-none">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
          </div>
        </nav>

      </div>

    </header>

    <!-- header -->

    <!-- content -->
    @yield('content')

    <!-- content -->

    <!-- footer -->

    <footer>

      <div class="container-fluid pt-5 pb-5">

        <div class="row d-flex justify-content-around">

          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" align="center">
            <ul class="list-inline">
              <li class="list-inline-item">
                <img src="/assets/images/logo/footer-logo.svg" class="img-fluid">
              </li>
              <li class="list-inline-item"><p class="footer mb-auto mt-auto">copyright 2020 <b>ValueTalk</b> - all rights reserved</p></li>
            </ul>
          </div>

          <div class="col-5 col-sm-4 col-md-4 col-lg-12 col-xl-12" align="center">
            <div>
              <img src="/assets/images/logo/wowitel.svg" class="img-fluid">
            </div>
          </div>

        </div>

      </div>

    </footer>

    <!-- footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="/assets/js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  </body>
