<?php $nav_terms = 'active'; ?>

@extends('master')

    <!-- content -->

      <!-- sect 1 -->
@section('title','Persyaratan dan Kondisi')

@section('content')
      <div class="container-fluid mb-5">

        <div class="container col-12 col-sm-12 col-md-12 col-lg-12 col-xl-7 pt-5">

          <h2 class="desc text-center mb-5">
            Persyaratan dan <br>Kondisi Berlangganan
          </h2>

          <ul class="list-unstyled pt-5">

            <li class="mb-5">
              <h5 class="terms-subs mb-4">RAHASIA PRIBADI</h5>
              <p class="desc-2"><b>VALUE TALK</b> tidak menjual atau menyewakan informasi pribadi Anda.<br><b>Valuetalk.id</b> hanya mengumpulkan informasi pribadi yang diperlukan untuk memproses dan memenuhi pesanan Anda dan mengirimkan informasi promosi yang Anda minta.<br>Setiap informasi yang dikumpulkan hanya digunakan untuk penggunaan khusus ini dan tidak diizinkan untuk tujuan lain apa pun.</p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">PENETAPAN HARGA</h5>
              <p class="desc-2">Layanan yang dibeli dari situs web kami diproses melalui keranjang belanja kami sendiri dan dipenuhi secara digital. Harga kami tercantum dalam Rupiah dan sudah termasuk pajak.<br>Setelah pembayaran Anda disetujui, kami akan mulai memproses pesanan Anda dan akan mengirimkan konfirmasi melalui alamat email yang Anda berikan dengan nomor telepon baru dan kode yang sesuai.</p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">PEMBAYARAN</h5>
              <p class="desc-2">Anda dapat membeli dari situs web ini dengan menggunakan salah satu opsi pembayaran yang ditawarkan melalui keranjang belanja kami.<br><b>Valuetalk.id</b> berhak memperbarui prosedur pembayarannya setiap saat tanpa pemberitahuan sebelumnya kepada Anda.</p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">PENGIRIMAN DAN KONFIRMASI PENGIRIMAN</h5>
              <p class="desc-2"><b>Konfirmasi pengiriman:</b><br>Anda akan menerima email konfirmasi pesanan Anda melalui alamat email yang Anda berikan dengan nomor telepon baru dan kode yang sesuai.<br>Jika Anda memerlukan bantuan tambahan dengan pesanan Anda, harap balas email konfirmasi atau ke <b>info@valuetalk.id</b></p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">KEBIJAKAN PENGEMBALIAN</h5>
              <p class="desc-2"><b>VALUE TALK</b> TIDAK memiliki jaminan uang kembali. Semua pembelian layanan kami bersifat final setelah pesanan layanan diselesaikan, di mana pelanggan akan diberitahu melalui email. Nikmati layanan gratis selama 14 (empat belas hari) hari ini untuk mencoba layanan <b>Value Talk.</b></p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">PEMBERITAHUAN HUKUM</h5>
              <p class="desc-2 mb-4">Semua pemberitahuan dari <b>Value Talk.id</b> kepada Anda dapat diposting di situs Web kami dan akan dianggap dikirimkan dalam waktu tiga puluh (30) hari setelah diposting.<br>Pemberitahuan dari Anda kepada <b>Value Talk</b> harus dilakukan dengan:</p>
              <p class="desc-2 mb-4"><b>EMAIL: <br>info@valuetalk.id</b></p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">PEMBERITAHUAN HAK CIPTA</h5>
              <p class="desc-2">Semua konten yang muncul di situs Web ini adalah milik <b>valuetalk.id</b><br>
              Hak Cipta ©2020 <b>Valuetalk.id</b> Seluruh hak cipta. Sebagai pengguna, Anda hanya berwenang untuk melihat, menyalin, mencetak, dan mendistribusikan dokumen di situs Web ini selama (1) dokumen digunakan untuk tujuan informasi saja, dan (2) salinan dokumen (atau bagiannya). ) termasuk pemberitahuan hak cipta berikut: Hak Cipta ©2020 <b>Valuetalk.id</b> Semua hak dilindungi undang-undang.
              </p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">MEREK DAGANG</h5>
              <p class="desc-2">Semua merek, produk, layanan, dan nama proses yang muncul di situs Web ini adalah merek dagang dari pemegangnya masing-masing. Referensi atau penggunaan produk, layanan, atau proses tidak menyiratkan rekomendasi, persetujuan, afiliasi, atau sponsor dari produk, layanan, atau proses tersebut oleh <b>valuetalk.id</b> Tidak ada yang terkandung di sini akan ditafsirkan sebagai memberikan implikasi, estoppel, atau lisensi atau hak di bawah paten, hak cipta, merek dagang, atau hak kekayaan intelektual lainnya dari valuetalk.id atau pihak ketiga, kecuali sebagaimana yang diberikan di sini.</p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">SYARAT PENGGUNAAN</h5>
              <p class="desc-2">Situs ini mungkin berisi pemberitahuan kepemilikan dan informasi hak cipta lainnya, yang ketentuannya harus dipatuhi dan diikuti. Informasi di situs ini mungkin mengandung ketidakakuratan teknis atau kesalahan ketik. Informasi, termasuk harga dan ketersediaan produk, dapat diubah atau diperbarui tanpa pemberitahuan. <b>Valuetalk.id</b> dan anak perusahaannya berhak untuk menolak layanan, menghentikan akun, dan / atau membatalkan pesanan atas kebijakannya sendiri, termasuk, tanpa batasan, jika valuetalk.id meyakini bahwa perilaku pelanggan melanggar hukum yang berlaku atau merugikan kepentingan valuetalk.id dan anak perusahaannya.</p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">SYARAT PENGGUNAAN</h5>
              <p class="desc-2 mb-0">Konten yang termasuk dalam situs web ini telah dikumpulkan dari berbagai sumber dan dapat berubah tanpa pemberitahuan seperti halnya produk, program, penawaran, atau informasi teknis yang dijelaskan dalam situs web ini. Valuetalk.id tidak membuat pernyataan atau jaminan apa pun mengenai kelengkapan, kualitas, atau kecukupan Situs Web atau Konten, atau kesesuaian, fungsionalitas, atau pengoperasian situs Web ini, layanan yang ditawarkan, atau Kontennya. Dengan menggunakan situs Web ini, Anda menanggung risiko bahwa Konten di situs Web ini mungkin tidak akurat, tidak lengkap, menyinggung, atau mungkin tidak memenuhi kebutuhan dan persyaratan Anda.</p>
              <p class="desc-2 mb-0">Valuetalk.id SECARA KHUSUS MENOLAK SEMUA JAMINAN, TERSURAT MAUPUN TERSIRAT, TERMASUK TANPA BATASAN JAMINAN DAPAT DIPERDAGANGKAN, KESESUAIAN UNTUK TUJUAN TERTENTU, DAN KETIDAKPELANGGARAN SEHUBUNGAN DENGAN HALAMAN DAN KONTEN WEB INI. Valuetalk.id TIDAK AKAN BERTANGGUNG JAWAB ATAS KERUSAKAN KHUSUS, TIDAK LANGSUNG, INSIDENTAL, ATAU KONSEKUENSIAL BAHKAN JIKA PERUSAHAAN TELAH DIBERITAHU TENTANG KEMUNGKINAN KERUSAKAN TERSEBUT.</p>
              <p class="desc-2 mb-0">Informasi dan konten di server ini disediakan “sebagaimana adanya” tanpa jaminan apa pun, baik tersurat maupun tersirat, termasuk namun tidak terbatas pada jaminan tersirat tentang kelayakan jual, kesesuaian untuk tujuan tertentu, dan non-pelanggaran.</p>
              <p class="desc-2 mb-0">Referensi dan deskripsi produk atau layanan dalam materi situs Web disediakan “sebagaimana adanya” tanpa jaminan apa pun, baik tersurat maupun tersirat. Valuetalk.id tidak bertanggung jawab atas kerusakan apa pun, termasuk kerusakan konsekuensial apa pun, dalam bentuk apa pun yang mungkin diakibatkan oleh pengguna dari penggunaan materi di situs Web ini atau salah satu produk atau layanan yang dijelaskan di sini.</p>
              <p class="desc-2 mb-0">Dimasukkannya materi di server ini tidak menyiratkan dukungan apa pun oleh Valuetalk.id, yang tidak membuat jaminan apa pun sehubungan dengan materi pokok materi server yang diiklankan.</p>
              <p class="desc-2">Ada kemungkinan bahwa materi server dapat mencakup ketidakakuratan atau kesalahan. Selain itu, ada kemungkinan bahwa penambahan, penghapusan, dan perubahan yang tidak sah dapat dilakukan oleh pihak ketiga ke materi server. Meskipun Valuetalk.id mencoba untuk memastikan integritas dan keakuratan materi server, tidak ada jaminan tentang kebenaran atau keakuratannya.</p>
              </p>
            </li>

            <li class="mb-5">
              <h5 class="terms-subs mb-4">LAIN-LAIN</h5>
              <p class="desc-2 mb-0"><b>TIDAK BERLAKU DI MANA DILARANG:</b> Meskipun informasi di situs Web ini dapat diakses di seluruh dunia, semua produk atau layanan yang dibahas dalam situs Web ini mungkin tidak tersedia untuk semua orang atau di semua lokasi geografis atau yurisdiksi. Valuetalk.id dan pengiklan masing-masing berhak untuk membatasi penyediaan produk atau layanan mereka untuk setiap orang, wilayah geografis, atau yurisdiksi yang mereka inginkan dan untuk membatasi jumlah produk atau layanan yang mereka sediakan. Setiap tawaran untuk produk atau layanan apa pun yang dibuat dalam materi di situs web ini tidak berlaku jika dilarang.</p>
              <p class="desc-2 mb-0"><b>HUKUM YANG MENGATUR:</b> Dalam kasus litigasi kedua belah pihak setuju bahwa Hukum Negara pendaftaran bisnis Valuetalk.id akan berlaku dan kedua belah pihak harus menyetujui yurisdiksi pengadilan Negara tersebut, atau dalam hal keragaman kewarganegaraan.</p>
              <p class="desc-2"><b>LAIN-LAIN:</b> Syarat dan Ketentuan merupakan keseluruhan perjanjian antara Anda dan Valuetalk.id sehubungan dengan situs web ini dan layanan yang ditawarkan. Syarat dan Ketentuan menggantikan semua komunikasi dan proposal sebelumnya atau pada saat yang sama, baik elektronik, lisan atau tertulis antara Anda dan Valuetalk.id sehubungan dengan situs Web ini. Modifikasi Syarat dan Ketentuan tidak akan efektif kecuali diizinkan oleh Valuetalk.id Jika ada ketentuan dari Syarat dan Ketentuan yang ditemukan bertentangan dengan hukum, maka ketentuan tersebut harus dibangun sedemikian rupa untuk mencerminkan, sebanyak mungkin, maksud para pihak, dengan ketentuan lainnya tetap utuh. kekuatan dan efek.</p>
              </p>
            </li>

          </ul>

        </div>

      </div>

      <!-- sect 1 -->

      <!-- sect 2 -->

      <div class="container-fluid bg-value-talk">

        <div class="container pt-5 pb-5">
          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <h5 class="terms-desc mb-1">
                Ada pertanyaan?
              </h5>
              <h5 class="terms-desc mb-1">
                Jangan ragu untuk menghubungi kami
              </h5>
              <h5 class="terms-desc mb-5">
                untuk mendapatkan bantuan
              </h5>
              <div align="center">
                <a href="daftar.html" class="btn btn-secondary btn-daftar-3">daftar</a>
              </div>
            </div>
          </div>
        </div>

      </div>

      <!-- sect 2 -->

    <!-- content -->

   @endsection
