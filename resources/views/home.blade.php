<?php $nav_home = 'active'; ?>
@extends('master')

@section('title','Homepage')
    <!-- content -->

      <!-- sect 1 -->
@section('content')
      <div class="container-fluid bg-sect-1 d-flex align-items-center">
        
        <div class="container">
          
          <div class="jumbotron bg-transparent p-0">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
                <h1 class="display-4">Tanpa Batas!</h1>
                <p class="lead">Nelp ke Seluruh Dunia</p>
                <hr class="my-4 border-0">
                <p class="desc">
                  Nikmati <i>nelp, texting, browsing</i> tanpa batas ke seluruh dunia dengan <b>biaya bulanan <span class="break-price">Rp. 199,000</span></b>. Beneran!
                </p>
                <p class="lead mb-5">
                  <a class="btn btn-primary btn-lg btn-daftar-1" href="daftar.html" role="button">Daftar</a>
                </p>
                <div class="row">
                  <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                    <a href="#">

                          <img class="mt-auto mb-auto" src="/assets/images/logo/playstore.svg" alt="Play Store">

                      </a>
                  
                  </div>
                  <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-4">
                   <a href="#">

                          <img class="mt-auto mb-auto ml-4" src="/assets/images/logo/appstore.svg" alt="App Store">

                      </a>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>

      <!-- sect 1 -->

      <!-- sect 2 -->

      <div class="container-fluid mt-5">
        
        <div class="row">
          
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5 offset-xl-1 mb-5">
            
            <img src="/assets/images/content/content-2.svg" class="img-fluid">

          </div>

          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3 offset-xl-1 mt-auto mb-auto">
            
            <h2 class="desc">
              Nelpon ke <br>Seluruh Dunia
            </h2>

            <p class="desc-1">Nelp dengan kualitas suara yang jernih ke seluruh dunia.</p>
            <p class="desc-2">Yang Anda perlu cuma koneksi mobile data atau wifi dan Anda sudah bisa nelp ke seluruh dunia, tanpa ada biaya roaming dan tanpa limit waktu lagi.<br>Hubungi relasi, teman Anda dari tempat tinggal Anda.</p>

          </div>

        </div>

      </div>

      <!-- sect 2 -->

      <!-- sect 3 -->

      <div class="container-fluid bg-sect-3">
        
        <div class="container pt-5 pb-5">
          
          <div class="row">
            
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-2 offset-xl-1 m-auto">
              
              <h2 class="desc position-txt-responsive">Prosesnya Sangat Mudah</h2>

            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 offset-xl-1 mt-auto mb-auto">
              
              <div class="row">
                
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                  
                  <div class="card border-0 bg-transparent">
                    
                    <div class="card-header border-bottom-0 text-center bg-transparent pb-0">
                      <label class="circle-card-1 rounded-circle mb-0">
                        1
                      </label>
                    </div>
                    <div class="card-body bg-transparent pb-0">
                      <div align="center">
                        <img src="/assets/images/content/card-proses-1.png" class="img-card img-fluid">
                      </div>
                    </div>
                    <div class="card-footer border-top-0 bg-transparent">
                      <h3 class="desc text-center">
                        Daftar
                      </h3>
                      <p class="desc-2 text-center">
                        untuk mendapatkan extension dan password
                      </p>
                    </div>

                  </div>

                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                  
                  <div class="card border-0 bg-transparent">
                    
                    <div class="card-header border-bottom-0 text-center bg-transparent pb-0">
                      <label class="circle-card-1 rounded-circle mb-0">
                        2
                      </label>
                    </div>
                    <div class="card-body bg-transparent pb-0">
                      <div align="center">
                        <img src="/assets/images/content/card-proses-2.png" class="img-card img-fluid">
                      </div>
                    </div>
                    <div class="card-footer border-top-0 bg-transparent">
                      <h3 class="desc text-center">
                        Login
                      </h3>
                      <p class="desc-2 text-center">
                        melalui aplikasi <b>VALUE TALK</b> yang bisa di install di <b>App Store (IOS)</b> dan <b>Play Store (Android)</b>
                      </p>
                    </div>

                  </div>

                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                  
                  <div class="card border-0 bg-transparent">
                    
                    <div class="card-header border-bottom-0 text-center bg-transparent pb-0">
                      <label class="circle-card-1 rounded-circle mb-0">
                        3
                      </label>
                    </div>
                    <div class="card-body bg-transparent pb-0">
                      <div align="center">
                        <img src="/assets/images/content/card-proses-3.png" class="img-card img-fluid">
                      </div>
                    </div>
                    <div class="card-footer border-top-0 bg-transparent">
                      <h3 class="desc text-center">
                        Daftar
                      </h3>
                      <p class="desc-2 text-center">
                        untuk mendapatkan extension dan password
                      </p>
                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

      <!-- sect 3 -->

      <!-- sect 4 -->

      <div class="container-fluid mt-5 mb-5">
        
        <div class="container">
          
          <h2 class="desc text-center">Layanan ValueTalk</h2>
          <p class="desc color-subs text-center mb-1">
            Dengan biaya bulanan <span class="break-price"><b>Rp. 199,000,</b></span>
          </p>
          <p class="desc color-subs text-center mb-5">ini yang Anda dapatkan:</p>

          <div class="row card-value">
            
            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
              
              <div class="card border-0 w-75">
                
                <div class="card-header border-bottom-0 bg-transparent pb-0 pt-4">
                  <div align="center">
                    <img src="/assets/images/content/card-value-1.png" class="img-card img-fluid">
                  </div>
                </div>

                <div class="card-body bg-transparent pb-2">
                  <div align="center">
                    <p class="desc-2 w-75 text-center">
                      Telp ke luar negeri, ke no manapun (telp selular dan telp rumah atau kantor)
                    </p>
                  </div>
                </div>

              </div>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
              
              <div class="card border-0 w-75">
                
                <div class="card-header border-bottom-0 bg-transparent pb-0 pt-4">
                  <div align="center">
                    <img src="/assets/images/content/card-value-2.png" class="img-card img-fluid">
                  </div>
                </div>

                <div class="card-body bg-transparent pb-2">
                  <div align="center">
                    <p class="desc-2 w-75 text-center">
                      No telp selular Anda akan muncul sebagai Caller ID
                    </p>
                  </div>
                </div>

              </div>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
              
              <div class="card border-0 w-75">
                
                <div class="card-header border-bottom-0 bg-transparent pb-0 pt-4">
                  <div align="center">
                    <img src="/assets/images/content/card-value-3.png" class="img-card img-fluid">
                  </div>
                </div>

                <div class="card-body bg-transparent pb-2">
                  <div align="center">
                    <p class="desc-2 w-75 text-center">
                      Private Calling (sembunyikan no Anda di Caller ID)
                    </p>
                  </div>
                </div>

              </div>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
              
              <div class="card border-0 w-75">
                
                <div class="card-header border-bottom-0 bg-transparent pb-0 pt-4">
                  <div align="center">
                    <img src="/assets/images/content/card-value-4.png" class="img-card img-fluid">
                  </div>
                </div>

                <div class="card-body bg-transparent pb-2">
                  <div align="center">
                    <p class="desc-2 w-75 text-center">
                      Kirim pesan (texting) tanpa batas dan tanpa biaya tambahan
                    </p>
                  </div>
                </div>

              </div>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
              
              <div class="card border-0 w-75">
                
                <div class="card-header border-bottom-0 bg-transparent pb-0 pt-4">
                  <div align="center">
                    <img src="/assets/images/content/card-value-5.png" class="img-card img-fluid">
                  </div>
                </div>

                <div class="card-body bg-transparent  pb-2">
                  <div align="center">
                    <p class="desc-2 w-75 text-center">
                      Browsing ke website legal manapun tanpa batasan
                    </p>
                  </div>
                </div>

              </div>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
              
              <div class="card border-0 w-75">
                
                <div class="card-header border-bottom-0 bg-transparent pb-0 pt-4">
                  <div align="center">
                    <img src="/assets/images/content/card-value-6.png" class="img-card img-fluid">
                  </div>
                </div>

                <div class="card-body bg-transparent pb-2">
                  <div align="center">
                    <p class="desc-2 w-75 text-center">
                      Pilihan koneksi (Wifi, Data, atau keduanya)
                    </p>
                  </div>
                </div>

              </div>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
              
              <div class="card border-0 w-75">
                
                <div class="card-header border-bottom-0 bg-transparent pb-0 pt-4">
                  <div align="center">
                    <img src="/assets/images/content/card-value-7.png" class="img-card img-fluid">
                  </div>
                </div>

                <div class="card-body bg-transparent pb-2">
                  <div align="center">
                    <p class="desc-2 w-75 text-center">
                      Nelp ke sesame pengguna Value Talk dengan mengunakan nomer extension
                    </p>
                  </div>
                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

      <!-- sect 4 -->

      <!-- sect 5 -->

      <div class="container-fluid bg-value-talk">
        
        <div class="container pt-5 pb-5">
          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <h4 class="desc mb-1">
                Dengan biaya <span class="break-price">Rp. 199,000</span> per bulan, kurang dari <span class="break-price">Rp. 10,000</span> per hari!
              </h4>
              <h4 class="desc mb-5">
                Tanpa Batas! Tanpa Kontrak!
              </h4>
              <div align="center">
                <small class="desc">
                  Coba gratis 14 hari disini
                </small>
                <br>
                <a href="daftar.html" class="btn btn-secondary btn-daftar-3">daftar</a>
              </div>
            </div>
          </div>
        </div>

      </div>

      <!-- sect 5 -->

    <!-- content -->

@endsection