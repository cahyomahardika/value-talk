<?php $nav_registration = 'active'; ?>

@extends('master')

    <!-- content -->

      <!-- sect 1 -->
  @section('title','Daftar')
  @section('content')
      <form>

        <div class="container-fluid h80-vh d-flex align-items-center mb-5">
          
          <div class="container">
            
            <div class="row">
              
              <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
                <h1 class="display-4 mb-5">Daftar</h1>
                <img src="/assets/images/content/daftar.png" class="img-fluid">
              </div>

              <div class="col-12 col-sm-12 col-md-6 col-lg-9 col-xl-7 offset-xl-1">
                
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <input type="text" class="form-control" id="inputNamaD" placeholder="Nama Depan">
                    </div>
                    <div class="form-group col-md-6">
                      <input type="text" class="form-control" id="inputNamaB" placeholder="Nama Belakang">
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="inputNamaP" placeholder="Nama Perusahaan">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="inputNamaN" placeholder="Nama Negara">
                  </div>
                  <div class="form-group">
                    <input type="number" class="form-control" id="inputNoKTP" placeholder="Nomor KTP">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input type="number" class="form-control" id="inputNoHP" placeholder="No HP yang dipakai untuk layanan Value Talk">
                  </div>
                  <div class="form-group">
                    <textarea class="form-control" id="exampleFormControlAlamat" rows="3" placeholder="Alamat Rumah / Kantor"></textarea>
                  </div>
                  <div class="form-group">
                    <input type="number" class="form-control" id="inputKodeP" placeholder="Kode Pos">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                  </div>

              </div>

            </div>

          </div>

        </div>

        <div class="container-fluid bg-brown">
          
          <div class="container pt-5 pb-5">
            
            <div class="row">
              
              <div class="col-12 col-sm-12 col-md-6 col-lg-3 col-xl-3">
                <h2 class="desc mb-5">Order Anda</h2>
              </div>

              <div class="col-12 col-sm-12 col-md-6 col-lg-9 col-xl-7 offset-xl-1">

                <div class="d-none">

                  <ul class="platform border-bottom-head-platform">

                    <li class="head">Layanan</li>

                    <li class="head">Subtotal</li>

                  </ul>

                  <ul class="platform border-bottom-body-platform">

                    <li class="desc">ValueTalk x1</li>

                    <li class="desc">Rp. 199.000 / bulan</li>

                  </ul>

                  <ul class="platform border-bottom-body-platform">

                    <li class="desc">Sub Total</li>

                    <li class="desc subs">Rp. 199.000</li>

                  </ul>

                  <ul class="platform subs-total">

                      <li class="desc">Total</li>

                      <li class="desc subs">Rp. 199.000</li>

                  </ul>

                </div>

                <div class="calculator-price">

                    <div class="row border-bottom-head-platform">
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 head">Layanan</div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 head">Subtotal</div>
                    </div>

                    <div class="row border-bottom-body-platform">
                        <div class="product-name col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 desc">
                          <div class="btn-quantity">
                            <span class="align-middle">ValueTalk</span>
                            <button class="btn removeQuatity ml-2" type="button"> -</button>
                            <input type="text" class="quantityText" value="1" readonly="readonly" />
                            <button class="btn addContity" type="button"> +</button>
                          </div>
                        </div>
                        <div class="unit-price desc col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 d-none">199</div>
                        <div class="unitPrice1 desc col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 mt-auto mb-auto">Rp 199.000 / bulan</div>
                    </div>

                    <div class="row">
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 desc">
                        Sub Total
                      </div>
                      <div class="total-price desc subs col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 d-none">Rp 199.000</div>
                      <div class="totalPrice1 desc subs col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">Rp 199.000</div>
                    </div>

                    <div class="row subs-total">
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 desc">Total</div>
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 desc subs">
                        <div class="grand-total">Rp 199.000</div>
                      </div>
                    </div>

                </div>

              </div>

            </div>

          </div>

          <div class="container pb-5">

            <div class="row">
              
              <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <p class="desc-1">
                  Pembayaran melalui :
                </p>
              </div>

            </div>

            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-9">
                <ul class="list-inline value-talk">
                  <li class="list-inline-item">
                    <img src="/assets/images/content/gopay.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/qris.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/shopee_pay.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/visa.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/master.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/bca.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/bni.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/briva.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/mandiri.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/atm.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/alto.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/prima.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/indomaret.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/alfamidi.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/alfamart.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/ovo.png" class="img-client img-fluid">
                  </li>
                  <li class="list-inline-item">
                    <img src="/assets/images/content/bca_klik.png" class="img-client img-fluid">
                  </li>
                  <li>
                    <img src="/assets/images/content/akulaku.png" class="img-client img-fluid">
                  </li>
                </ul>
              </div>
            </div>

          </div>

        </div>

        <div class="container-fluid mb-5 mt-4">
          <div class="container pt-5 pb-5">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-4">
                <p class="desc-2">Data pribadi Anda akan digunakan untuk memproses pesanan Anda, mendukung pengalaman Anda di seluruh situs web ini, dan untuk tujuan lain yang dijelaskan dalam kebijakan privasi kami.</p>
                <div class="form-check check-value mb-5">
                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                  <label class="form-check-label align-middle" for="defaultCheck1">
                    Saya telah membaca syarat dan ketentuan dalam website ini.
                  </label>
                </div>
                <a class="btn btn-primary btn-lg btn-daftar-1" href="#" role="button">Proses</a>
              </div>
            </div>
          </div>
        </div>

      </form>

      <!-- sect 1 -->

    <!-- content -->
@endsection