<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route page navigation with template view master.blade.php */
Route::get('/', 'SiteController@home');
Route::get('/contact', 'SiteController@contact');
Route::get('/termscondition', 'SiteController@termscondition');
Route::get('/registration', 'SiteController@registration');

/* Route contact us */
Route::post('/contact/store','ContactController@store');
