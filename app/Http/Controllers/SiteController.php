<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    //
    public function home(){
		return view('home');
	}
 
	public function contact(){
		return view('contact');
	}
 
	public function termscondition(){
		return view('termscondition');
	}

    public function registration(){
		return view('registration');
	}
}
