<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ContactUs;

class ContactController extends Controller
{
    public function store(Request $request){
        // query builder laravel
        // DB::table('contactus')->insert([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'phone' => $request->phone,
        //     'message' => $request->message,
        // ]);

        //eloquent
        ContactUs::create([
    		'name' => $request->name,
    		'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
    	]);

        // redirect to home
        // Alert::success('Berhasil menghubungi kami, mohon untuk menunggu respon dari kami', 'Berhasil');
        alert()->success('Berhasil menghubungi kami, mohon untuk menunggu respon dari kami', 'Berhasil');
        return redirect('/');
    }
}
